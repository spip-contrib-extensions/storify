<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_core_/plugins/mots/lang/
if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'legend_story_lines'        => 'Chapitres',
	'label_type_center'         => 'Bloc centré',
	'label_type_two_rows'       => '2 colonnes',
	'label_type_two_rows_alt'   => '2 colonnes (alternée)',
	'label_type_two_rows_large' => '2 colonnes larges',
	'label_type_two_rows_large_alt' => '2 colonnes larges (alternée)',
	'label_type_photo_large'    => 'Photo large',
	'label_type_stats'          => 'Statistiques',
	'label_type_action'         => 'Call-to-action',

	'label_line_center_bloc_0' => 'Texte',
	'label_line_two_rows_bloc_0' => 'Texte',
	'label_line_two_rows_bloc_1' => 'Citation/photo',
	'label_line_photo_large_0' => 'Texte',
	'label_line_photo_large_1' => 'Photo',
	'label_line_stats_bloc_0' => 'Titre',
	'label_line_stats_bloc_1' => 'Chiffre 1',
	'label_line_stats_bloc_2' => 'Legende',
	'label_line_stats_bloc_3' => 'Chiffre 2',
	'label_line_stats_bloc_4' => 'Legende',
	'label_line_stats_bloc_5' => 'Chiffre 3',
	'label_line_stats_bloc_6' => 'Legende',
	'label_line_stats_bloc_7' => 'Chiffre 4',
	'label_line_stats_bloc_8' => 'Legende',

	'label_line_action_bloc_0' => 'Texte',
	'label_line_action_bloc_1' => 'Bouton',
	'label_line_action_bloc_2' => 'URL',

	'bouton_changer' => 'Changer',
	'bouton_ajouter' => 'Ajouter',
);

